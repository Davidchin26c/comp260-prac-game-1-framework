﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	public float beePeriod;
	public float minBeePeriod;
	public float MaxBeePeriod;
	public int nBees = 50;
	public BeeMove BeePrefab;
	public float xMin, yMin;
	public float width, height;
	// Use this for initialization
	void Start () {
			StartCoroutine(Colo());

	}

	public void DestroyBees(Vector2 centre, float radius){
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}
	// Update is called once per frame
	void Update () {
		



		
	}
	IEnumerator Colo(){
		for (int i =0; i < nBees; i++){
			beePeriod = Mathf.Lerp (minBeePeriod, MaxBeePeriod, Random.value);
			BeeMove bee = Instantiate (BeePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "Bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
			yield return new WaitForSeconds (beePeriod);
		}


	}
}
