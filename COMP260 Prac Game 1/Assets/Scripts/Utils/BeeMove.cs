﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed;
	private float turnSpeed;

	// Use this for initialization
	void Start () {
		PlayerMove player = 
			(PlayerMove) FindObjectOfType(typeof(PlayerMove));
		target = player.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);

	}
		
	public Transform target;

	public Vector2 heading = Vector3.right;

	// Update is called once per frame
	void Update () {
		Vector2 direction = target.position - transform.position;



		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);

		} else {

			heading = heading.Rotate (-angle);
		}
			
		transform.Translate (heading * speed * Time.deltaTime);

	
	
	}

	public ParticleSystem explosionPrefab;

	void OnDestroy(){
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy (explosion.gameObject.gameObject, explosion.duration);
	}
	void OnDrawGizmos() {

		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}
}
