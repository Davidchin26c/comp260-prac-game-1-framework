﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	private BeeSpawner beeSpawner;
	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
		
	}

	public Vector2 move;
	public Vector2 velocity;
	public float maxspeed = 5.0f;
	public string Xaxis;
	public string Yaxis;
	public float acceleration = 3.0f;
	public float brake = 5.0f;
	private float speed = 0.0f;
	public float turnSpeed = 30.0f;
	public float destroyRadius = 1.0f;



	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Fire1")) {
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}
		
		float turn = Input.GetAxis(Xaxis);
		transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime * -1 *speed);


		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(Yaxis);
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (speed > 0) {
				
				speed = speed - brake * Time.deltaTime;
				speed = Mathf.Clamp(speed, 0, maxspeed);
			} else {
				speed = speed + brake * Time.deltaTime;
				speed = Mathf.Clamp(speed, -maxspeed, 0);
			}
		}


		speed = Mathf.Clamp(speed, -maxspeed, maxspeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

		
	}
}
